package com.xhh.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @description
 * @author: weiXhh
 * @create: 2020-05-11 13:13
 **/
@Data
public class SpuBoundsTo {
    private Long spuId;
    private BigDecimal buyBounds;
    private BigDecimal growBounds;
}
