/**
  * Copyright 2020 bejson.com 
  */
package com.xhh.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Auto-generated: 2020-05-10 18:49:41
 *
 * @author
 */
@Data
public class MemberPrice {

    private Long id;
    private String name;
    private BigDecimal price;
}