package com.xhh.common.constant;

/**
 * @description
 * @author: weiXhh
 * @create: 2020-05-09 14:43
 **/
public class ProductConstant {
    public enum AttrEnum{
        ATTR_TYPE_BASE(1,"基本属性"),ATTR_TYPE_SALE(0,"销售属性");
        private int type;
        private String message;
        AttrEnum(int type, String message){
            this.type = type;
            this.message = message;
        }

        public int getType() {
            return type;
        }

        public String getMessage() {
            return message;
        }

    }

    public enum StatusEnum{
        NEW_SPU(0,"新建"),SPU_UP(1,"商品上架"),SPU_DOWN(2,"商品下架");
        private int type;
        private String message;
        StatusEnum(int type, String message){
            this.type = type;
            this.message = message;
        }

        public int getType() {
            return type;
        }

        public String getMessage() {
            return message;
        }

    }
}
