package com.xhh.common.constant;

/**
 * @description
 * @author: weiXhh
 * @create: 2020-05-09 14:43
 **/
public class WareConstant {
    private static String tag = "v1.0";
    public enum PurchaseStatusEnum {
        CREATED(0,"新建"),ASSIGNED(1,"已分配"),
        RECEIVE(2,"已领取"),FINISH(3,"已完成"),
        HASERROR(4,"有异常");
        private int type;
        private String message;
        PurchaseStatusEnum(int type, String message){
            this.type = type;
            this.message = message;
        }

        public int getType() {
            return type;
        }

        public String getMessage() {
            return message;
        }
    }
    public enum PurchaseDetailStatusEnum{
        CREATED(0,"新建"),ASSIGNED(1,"已分配"),
        BUYING(2,"正在采购"),FINISH(3,"已完成"),
        HASERROR(4,"采购失败");
        private int type;
        private String message;
        PurchaseDetailStatusEnum(int type, String message){
            this.type = type;
            this.message = message;
        }

        public int getType() {
            return type;
        }

        public String getMessage() {
            return message;
        }
    }
}
