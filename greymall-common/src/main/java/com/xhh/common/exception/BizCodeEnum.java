package com.xhh.common.exception;

/**
 * @description
 * 错误码列表
 * 10:通用
 *     001:参数格式校验
 *     002:短信验证码频率太高
 * 11:商品
 * 12:订单
 * 13:购物侧
 * 14:物流
 * 15:用户
 * 21:库存
 *
 *
 * @author: weiXhh
 * @create: 2020-05-07 14:37
 **/
public enum BizCodeEnum {
    UNKNOWN_EXCEPTION(10000,"未知错误异常"),
    VALID_EXCEPTION(10001, "参数校验错误"),
    TO_MANY_REQUEST(10002, "请求流量过大"),
    SMS_CODE_EXCEPTION(10002, "验证码获取频率太高，稍后再试"),
    PRODUCT_UP_EXCEPTION(11000, "商品上架异常"),
    USER_EXIST_EXCEPTION(15001, "用户存在异常"),
    PHONE_EXIST_EXCEPTION(15002, "手机号存在异常"),
    NO_STOCK_EXCEPTION(21000,"商品库存不足"),
    LOGINACCT_PASSWORD_INVALID_EXCEPTION(15003, "账号密码错误");

    private final int code;
    private final String errorMessage;
    BizCodeEnum(int code, String errorMessage){
        this.code = code;
        this.errorMessage = errorMessage;
    }

    public int getCode() {
        return code;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
