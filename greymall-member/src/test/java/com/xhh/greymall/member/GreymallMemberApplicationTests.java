package com.xhh.greymall.member;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.digest.Md5Crypt;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootTest
class GreymallMemberApplicationTests {

    @Test
    void contextLoads() {
        //123456的md5->e10adc3949ba59abbe56e057f20f883e,抗修改性
        //可暴力破解-》彩虹表
//        String s = DigestUtils.md5Hex("123456");
//        System.out.println(s);

        //MD5不能直接进行密码的加密存储
//        "123456" + System.currentTimeMillis();

        //盐值加密：随机值 加盐：$1$+8位字符
        //对密码的验证：123456进行盐值（去数据库查）加密
//        String s1 = Md5Crypt.md5Crypt("123456".getBytes());
//        System.out.println(s1);
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encode = bCryptPasswordEncoder.encode("123456");


        boolean matches = bCryptPasswordEncoder.matches("123456", "$2a$10$Wab7zLKVKDqa0DRdv7D8leP3NXOGtfWzA133zEzvfW2Jb3N31eFku");
        System.out.println(encode + "=>" + matches);
    }

}
