package com.xhh.greymall.member.exception;

/**
 * @description:
 * @author: wei-xhh
 * @create: 2020-07-20
 */
public class PhoneExistException extends RuntimeException {
    public PhoneExistException() {
        super("手机号存在");
    }
}
