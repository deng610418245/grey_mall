package com.xhh.greymall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xhh.common.utils.PageUtils;
import com.xhh.greymall.member.entity.MemberEntity;
import com.xhh.greymall.member.exception.PhoneExistException;
import com.xhh.greymall.member.exception.UsernameExistException;
import com.xhh.greymall.member.vo.MemberLoginVo;
import com.xhh.greymall.member.vo.MemberRegistVo;
import com.xhh.greymall.member.vo.SocialUser;

import java.io.IOException;
import java.util.Map;

/**
 * 会员
 *
 * @author genghui
 * @email 484613733@qq.com
 * @date 2020-05-02 17:23:10
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void regist(MemberRegistVo vo);

    void checkPhoneUnique(String phone) throws PhoneExistException;

    void checkUsernameUnique(String username) throws UsernameExistException;

    MemberEntity login(MemberLoginVo vo);

    MemberEntity oauthLogin(SocialUser socialUser) throws IOException;
}

