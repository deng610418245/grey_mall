package com.xhh.greymall.product;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xhh.greymall.product.dao.AttrGroupDao;
import com.xhh.greymall.product.dao.SkuSaleAttrValueDao;
import com.xhh.greymall.product.entity.BrandEntity;
import com.xhh.greymall.product.service.BrandService;
import com.xhh.greymall.product.service.CategoryService;
import com.xhh.greymall.product.vo.SkuItemSaleAttrVo;
import com.xhh.greymall.product.vo.SkuItemVo;
import com.xhh.greymall.product.vo.SpuItemAttrGroupVo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Slf4j
@SpringBootTest
class GreymallProductApplicationTests {

    @Autowired
    BrandService brandService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    RedissonClient redissonClient;

    @Autowired
    AttrGroupDao attrGroupDao;

    @Autowired
    SkuSaleAttrValueDao skuSaleAttrValueDao;

    @Test
    void test(){
//        List<SpuItemAttrGroupVo> attrGroupWithAttrsBySpuId = attrGroupDao.getAttrGroupWithAttrsBySpuId(8L, 225L);
//        System.out.println(attrGroupWithAttrsBySpuId);
        List<SkuItemSaleAttrVo> saleAttrsBySpuId = skuSaleAttrValueDao.getSaleAttrsBySpuId(8L);
        System.out.println(saleAttrsBySpuId);
    }

    @Test
    void testRedissonClient(){
        System.out.println(redissonClient);
    }

    @Test
    void testStringRedisTemplate(){
        ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
        ops.set("hello", "world_" + UUID.randomUUID().toString());

        String hello = ops.get("hello");

        System.out.println("hello对应的数据：" + hello);
    }

    @Test
    void testParentCatId(){
        Long[] catelogPath = categoryService.findCatelogPath(255L);
        log.info("完整路径:{}", Arrays.asList(catelogPath));
    }

    @Test
    void contextLoads() {
//        BrandEntity brandEntity = new BrandEntity();
//        brandEntity.setBrandId(1L);
//        brandEntity.setDescript("苹果");
//        brandService.updateById(brandEntity);

//        brandEntity.setDescript("");
//        brandEntity.setName("华为");
//        brandService.save(brandEntity);

        List<BrandEntity> list = brandService.list(new QueryWrapper<BrandEntity>().eq("brand_id", 1L));
        list.forEach((item)->{
            System.out.println(item);
        });
    }

}
