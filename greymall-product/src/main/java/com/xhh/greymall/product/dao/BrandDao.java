package com.xhh.greymall.product.dao;

import com.xhh.greymall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import javax.annotation.Resource;

/**
 * 品牌
 * 
 * @author genghui
 * @email 484613733@qq.com
 * @date 2020-05-02 16:45:41
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
