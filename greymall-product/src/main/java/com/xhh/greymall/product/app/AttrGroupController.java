package com.xhh.greymall.product.app;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.xhh.greymall.product.entity.AttrEntity;
import com.xhh.greymall.product.service.AttrAttrgroupRelationService;
import com.xhh.greymall.product.service.CategoryService;
import com.xhh.greymall.product.vo.AttrGroupRelationVO;
import com.xhh.greymall.product.vo.AttrGroupWithAttrsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.xhh.greymall.product.entity.AttrGroupEntity;
import com.xhh.greymall.product.service.AttrGroupService;
import com.xhh.common.utils.PageUtils;
import com.xhh.common.utils.R;



/**
 * 属性分组
 *
 * @author genghui
 * @email 484613733@qq.com
 * @date 2020-05-02 16:45:41
 */
@RestController
@RequestMapping("product/attrgroup")
public class AttrGroupController {
    @Autowired
    private AttrGroupService attrGroupService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private AttrAttrgroupRelationService relationService;

    //attrgroup/0/withattr
    @GetMapping("/{catelogId}/withattr")
    public R getAttrGroupWithAttrs(@PathVariable("catelogId") Long catelogId){
        List<AttrGroupWithAttrsVO> list = attrGroupService.getAttrGroupWithAttrsByCatelogId(catelogId);
        return R.ok().put("data", list);
    }

    @PostMapping("/attr/relation")
    public R attrRelation(@RequestBody List<AttrGroupRelationVO> vos){
        relationService.saveBatch(vos);
        return R.ok();
    }

    /**
     * 查询该组下所有的attr
     * @param attrgroup
     * @return
     */
    @GetMapping("/{attrgroup}/attr/relation")
    public R attrRelation(@PathVariable("attrgroup") Long attrgroup){
        List<AttrEntity> entities = attrGroupService.queryAttrRelation(attrgroup);
        return R.ok().put("data", entities);
    }

    /**
     * 查询该组没有关联的属性
     * @param attrgroup
     * @return
     */
    @GetMapping("/{attrgroup}/noattr/relation")
    public R attrNoRelation(@PathVariable("attrgroup") Long attrgroup,
                            @RequestParam Map<String, Object> params){
        PageUtils page = attrGroupService.queryNoAttrRelation(attrgroup, params);
        return R.ok().put("page", page);
    }

    /**
     * 删除
     * @param vos
     * @return
     */
    @PostMapping("/attr/relation/delete")
    public R deleteRelation(@RequestBody AttrGroupRelationVO[] vos){
        attrGroupService.deleteRelation(vos);
        return R.ok();
    }



    /**
     * 列表
     */
    @RequestMapping("/list/{catId}")
    //@RequiresPermissions("product:attrgroup:list")
    public R list(@RequestParam Map<String, Object> params,
                  @PathVariable("catId") Long catId){
//        PageUtils page = attrGroupService.queryPage(params);

        PageUtils page = attrGroupService.queryPage(params, catId);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{attrGroupId}")
    //@RequiresPermissions("product:attrgroup:info")
    public R info(@PathVariable("attrGroupId") Long attrGroupId){
		AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);

        // 查询该id的[父/子/孙]
        Long catelogId = attrGroup.getCatelogId();
        Long[] catelogPath = categoryService.findCatelogPath(catelogId);

        attrGroup.setCatelogPath(catelogPath);

        return R.ok().put("attrGroup", attrGroup);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("product:attrgroup:save")
    public R save(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.save(attrGroup);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("product:attrgroup:update")
    public R update(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.updateById(attrGroup);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("product:attrgroup:delete")
    public R delete(@RequestBody Long[] attrGroupIds){
		attrGroupService.removeByIds(Arrays.asList(attrGroupIds));

        return R.ok();
    }

}
