package com.xhh.greymall.product.feign;

import com.xhh.common.to.SkuHasStockVO;
import com.xhh.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @description
 * @author: weiXhh
 * @create: 2020-06-23 09:45
 **/
@FeignClient("greymall-ware")
public interface WareFeignService {
    /**
     * 为了返回数据的转换
     * 1、R设计的时候可以加上泛型->后期出错->R是一个Map,不能使用泛型
     * 2、直接返回我们想要的结果
     * 3、自己封装解析结果
     * @param skuIds
     * @return
     */
    @PostMapping("/ware/waresku/hasstock")
    R getSkuHasStock(@RequestBody List<Long> skuIds);
}
