package com.xhh.greymall.product.feign;

import com.xhh.common.to.es.SkuEsModel;
import com.xhh.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @description
 * @author: weiXhh
 * @create: 2020-06-23 10:42
 **/
@FeignClient("greymall-search")
public interface SearchFeignService {

    @PostMapping("/search/save/product")
    R productStatusUp(@RequestBody List<SkuEsModel> skuEsModels);
}
