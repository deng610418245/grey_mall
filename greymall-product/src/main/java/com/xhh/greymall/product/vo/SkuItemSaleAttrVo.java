package com.xhh.greymall.product.vo;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @description
 * @author: wei-xhh
 * @create: 2020-07-17
 **/
@ToString
@Data
public class SkuItemSaleAttrVo {
    private Long attrId;
    private String attrName;
    private List<AttrValueWithSkuIdVo> attrValues;
}
