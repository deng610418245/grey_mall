/**
  * Copyright 2020 bejson.com 
  */
package com.xhh.greymall.product.vo;

import lombok.Data;
import lombok.ToString;

/**
 * Auto-generated: 2020-05-10 18:49:41
 *
 * @author
 */
@ToString
@Data
public class Attr {

    private Long attrId;
    private String attrName;
    private String attrValue;

}