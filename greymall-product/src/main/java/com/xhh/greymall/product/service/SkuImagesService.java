package com.xhh.greymall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xhh.common.utils.PageUtils;
import com.xhh.greymall.product.entity.SkuImagesEntity;

import java.util.List;
import java.util.Map;

/**
 * sku图片
 *
 * @author genghui
 * @email 484613733@qq.com
 * @date 2020-05-02 16:45:41
 */
public interface SkuImagesService extends IService<SkuImagesEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<SkuImagesEntity> getImagesBySkuId(Long skuId);
}

