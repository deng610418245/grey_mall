package com.xhh.greymall.product.feign.fallback;

import com.xhh.common.exception.BizCodeEnum;
import com.xhh.common.utils.R;
import com.xhh.greymall.product.feign.SeckillFeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @description:
 * @author: wei-xhh
 * @create: 2020-08-03
 */
@Component
@Slf4j
public class SeckillFeignServiceFallBack implements SeckillFeignService {
    @Override
    public R getSkuSeckillInfo(Long skuId) {
        log.info("熔断方法调用...getSkuSeckillInfo");
        return R.error(BizCodeEnum.TO_MANY_REQUEST.getCode(),BizCodeEnum.TO_MANY_REQUEST.getErrorMessage());
    }
}
