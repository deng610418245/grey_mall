/**
  * Copyright 2020 bejson.com 
  */
package com.xhh.greymall.product.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Auto-generated: 2020-05-10 18:49:41
 *
 * @author
 */
@Data
public class Bounds {

    private BigDecimal buyBounds;
    private BigDecimal growBounds;

}