package com.xhh.greymall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xhh.common.utils.PageUtils;
import com.xhh.greymall.product.entity.AttrGroupEntity;
import com.xhh.greymall.product.entity.CategoryEntity;
import com.xhh.greymall.product.vo.Catalog2VO;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author genghui
 * @email 484613733@qq.com
 * @date 2020-05-02 16:45:41
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CategoryEntity> listWithTree();

    void removeMenuByIds(List<Long> asList);

    /**
     *  找到完整路径[父/子/孙]
     * @param catelogId
     * @return
     */
    Long[] findCatelogPath(Long catelogId);

    void updateDetail(CategoryEntity category);

    /**
     * 获得1级分类
     * @return
     */
    List<CategoryEntity> getCatLevelOneList();

    /**
     * 获得分类的json
     * @return
     */
    Map<String, List<Catalog2VO>> getCatalogJson();
}

