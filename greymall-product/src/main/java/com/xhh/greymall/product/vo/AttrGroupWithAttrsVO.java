package com.xhh.greymall.product.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.xhh.greymall.product.entity.AttrEntity;
import lombok.Data;

import java.util.List;

/**
 * @description
 * @author: weiXhh
 * @create: 2020-05-10 16:46
 **/
@Data
public class AttrGroupWithAttrsVO {
    /**
     * 分组id
     */
    private Long attrGroupId;
    /**
     * 组名
     */
    private String attrGroupName;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 描述
     */
    private String descript;
    /**
     * 组图标
     */
    private String icon;
    /**
     * 所属分类id
     */
    private Long catelogId;

    /**
     * 该分组下的所有属性
     */
    private List<AttrEntity> attrs;

}
