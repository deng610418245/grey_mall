package com.xhh.greymall.product.vo;

import lombok.Data;

/**
 * @description
 * @author: wei-xhh
 * @create: 2020-07-18
 **/
@Data
public class AttrValueWithSkuIdVo {
    private String attrValue;
    private String skuIds;
}
