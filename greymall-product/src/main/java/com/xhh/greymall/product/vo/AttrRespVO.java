package com.xhh.greymall.product.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @description
 * @author: weiXhh
 * @create: 2020-05-09 10:31
 **/
@Data
public class AttrRespVO extends AttrVO {
    private String catelogName;
    private String groupName;
    private Long[] catelogPath;
}
