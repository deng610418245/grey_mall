package com.xhh.greymall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 1、使用nacos作为配置中心统一管理配置
 * 1).导入配置依赖
 *     <dependency>
 *         <groupId>com.alibaba.cloud</groupId>
 *         <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
 *     </dependency>
 * 2).创建一个bootstrap.properties
 *     spring.cloud.nacos.config.server-addr=127.0.0.1:8848
 *     spring.application.name=greymall-coupon
 * 3).需要给配置中心默认添加一个叫数据集(data id) greymall-coupon.properties：默认规则 应用名.properties
 * 4).给应用名.properties添加任何配置
 * 5).动态获取配置
 *    @RefreshScope：动态获取并刷新配置
 *    @Value("${配置项的名}") 获取配置信息
 */
@EnableDiscoveryClient
@SpringBootApplication
public class GreymallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(GreymallCouponApplication.class, args);
    }

}
