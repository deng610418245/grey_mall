package com.xhh.greymall.search.thread;

import java.util.concurrent.*;

/**
 * @description
 * @author: wei-xhh
 * @create: 2020-07-16
 **/
public class ThreadTest {
    public static ExecutorService executorService = Executors.newFixedThreadPool(10);

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        System.out.println("main....start.....");
//        CompletableFuture<Void> voidCompletableFuture = CompletableFuture.runAsync(() -> {
//            System.out.println("当前线程：" + Thread.currentThread().getId());
//            int i = 10 / 2;
//            System.out.println("运行结果：" + i);
//        }, executorService);
//        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
//            System.out.println("当前线程：" + Thread.currentThread().getId());
//            int i = 10 / 0;
//            System.out.println("运行结果：" + i);
//            return i;
//        }, executorService).whenComplete((res,exc)->{
//            System.out.println("异步任务成功完成了...结果是：" + res + ";异常是：" + exc);
//        }).exceptionally(throwable -> {
//            return 10;
//        });
        //方法执行完成后的处理
//        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
//            System.out.println("当前线程：" + Thread.currentThread().getId());
//            int i = 10 / 0;
//            System.out.println("运行结果：" + i);
//            return i;
//        }, executorService).handle((res,thr)->{
//            if(res != null){
//                return res * 2;
//            }
//            if(thr != null){
//                return 0;
//            }
//            return 0;
//        });
//        CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
//            System.out.println("当前线程：" + Thread.currentThread().getId());
//            int i = 10 / 4;
//            System.out.println("运行结果：" + i);
//            return i;
//        }, executorService).thenApplyAsync(res -> {
//            System.out.println("任务2启动了" + res);
//            return "Hello" + res;
//        }, executorService);
//        Integer integer = future.get();

        /**
         * 两个都完成
         */
//        CompletableFuture<Object> future01 = CompletableFuture.supplyAsync(() -> {
//            System.out.println("任务1线程：" + Thread.currentThread().getId());
//            int i = 10 / 4;
//            System.out.println("任务1结束：");
//            return i;
//        }, executorService);
//        CompletableFuture<Object> future02 = CompletableFuture.supplyAsync(() -> {
//            System.out.println("任务2线程：" + Thread.currentThread().getId());
//            try {
//                Thread.sleep(3000);
//                System.out.println("任务2结束");
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            return "Hello";
//        }, executorService);
//        future01.runAfterBothAsync(future02,()->{
//            System.out.println("任务3开始");
//        },executorService);
//        future01.thenAcceptBothAsync(future02, (f1,f2)->{
//            System.out.println("任务3开始，之前的结果" + f1 + "-->" + f2);
//        },executorService);
//        CompletableFuture<String> future = future01.thenCombineAsync(future02, (f1, f2) -> {
//            return f1 + ":" + f2 + "->haha";
//        }, executorService)
        /**
         * 两个任务只要有一个完成，我们就执行任务3
         */
//        future01.runAfterEitherAsync(future02,()->{
//            System.out.println("任务3开始，之前的结果");
//        }, executorService);
//        future01.acceptEitherAsync(future02, (res)->{
//            System.out.println("任务3开始，之前的结果" + res);
//        },executorService);
//        CompletableFuture<String> future = future01.applyToEitherAsync(future02, (res) -> {
//            System.out.println("任务3开始，之前的结果" + res);
//            return res.toString() + "->哈哈";
//        }, executorService);
        CompletableFuture<String> futureImg = CompletableFuture.supplyAsync(() -> {
            System.out.println("查询商品的图片信息");
            return "hello.jpg";
        },executorService);
        CompletableFuture<String> futureAttr = CompletableFuture.supplyAsync(() -> {
            System.out.println("查询商品的属性");
            return "黑色+256G";
        },executorService);
        CompletableFuture<String> futureDesc = CompletableFuture.supplyAsync(() -> {

            try {
                Thread.sleep(3000);
                System.out.println("查询商品介绍");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "华为";
        },executorService);
        CompletableFuture<Object> anyOf = CompletableFuture.anyOf(futureImg, futureAttr, futureDesc);
        anyOf.get(); //等待所有结果完成

        System.out.println("main....end....." + anyOf.get());
    }

    public void thread() {
        System.out.println("main....start.....");
        /**
         * 1. 继承Thread
         *         Thread01 thread = new Thread01();
         *           thread.start(); //启动线程
         *
         * 2. 实现Runnable接口
         *        Runnable01 runnable01 = new Runnable01();
         *         new Thread(runnable01).start();
         * 3. 实现Callable接口+FutureTask
         *       FutureTask<Integer> futureTask = new FutureTask<>(new Callable01());
         *         new Thread(futureTask).start();
         *         //阻塞等待整个线程执行完成，获取返回结果
         *         Integer integer = futureTask.get();
         * 4. 线程池
         *      给线程池直接提交任务。
         *          1、创建：
         *              1)、Executors
         *              2)、new ThreadPoolExecutor
         *
         * 区别：1,2不能得到返回值，3可以获取返回值
         *      1,2,3都不能控制资源
         *      4可以控制资源，性能稳定
         */

        //以后再业务代码里面，以上三种启动线程的方式都不用。应该【将所有的多线程异步任务都交给线程池执行】
//        new Thread(()->{
//            System.out.println("hello");
//        }).start();

        //当前系统中池只有一两个，每个异步任务，提交给线程池让他自己去执行就行。
//        executorService.execute(new Runnable01());
        /**
         * 七大参数
         * corePoolSize:[5]核心线程数[一直存在]；线程池，创建好以后就准备就绪的线程数量，就等待来接受异步任务去执行
         *     5个 Thread thread = new Thread();
         * maximumPoolSize[200]:最大线程数量；控制资源
         * keepAliveTime:存活时间。如果当前的线程数量大于core数量
         *      释放空闲的线程(maximumPoolSize-corePoolSize)。只要线程空闲大于指定的keepAliveTime;
         * unit:时间单位
         * BlockingQueue<Runnable> workQueue:阻塞队列。如果任务有很多，就会将目前多的任务放在队列里面。
         *                                  只要有线程空闲，就会取队列里取出新的任务继续
         * threadFactory:线程的创建工厂。
         * RejectedExecutionHandler handler:如果队列满了，按照我们指定的拒绝策略拒绝执行任务。
         *
         *
         */
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                5,
                200,
                10,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(100000),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy());
        System.out.println("main....end.....");
    }

    public static class Thread01 extends Thread {
        @Override
        public void run() {
            System.out.println("当前线程：" + Thread.currentThread().getId());
            int i = 10 / 2;
            System.out.println("运行结果：" + i);
        }
    }

    public static class Runnable01 implements Runnable {

        @Override
        public void run() {
            System.out.println("当前线程：" + Thread.currentThread().getId());
            int i = 10 / 2;
            System.out.println("运行结果：" + i);
        }
    }

    public static class Callable01 implements Callable<Integer> {

        @Override
        public Integer call() throws Exception {
            System.out.println("当前线程：" + Thread.currentThread().getId());
            int i = 10 / 2;
            System.out.println("运行结果：" + i);
            return i;
        }
    }
}
