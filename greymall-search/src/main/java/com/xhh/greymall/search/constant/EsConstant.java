package com.xhh.greymall.search.constant;

/**
 * @description
 * @author: weiXhh
 * @create: 2020-06-23 10:19
 **/
public class EsConstant {
    public static final String PRODUCT_INDEX = "greymall_product"; //sku数据在es中的索引
    public static final int PRODUCT_PAGE_SIZE = 8; //分页大小
}
