package com.xhh.greymall.search.vo;

import lombok.Data;

/**
 * @description
 * @author: wei-xhh
 * @create: 2020-07-15
 **/
@Data
public class BrandVo {
    private Long brandId;
    private String brandName;
}
