package com.xhh.greymall.search.service;

import com.xhh.common.to.es.SkuEsModel;

import java.io.IOException;
import java.util.List;

/**
 * @description
 * @author: weiXhh
 * @create: 2020-06-23 10:16
 **/
public interface ProductSaveService {

    boolean productStatusUp(List<SkuEsModel> skuEsModels) throws IOException;
}
