package com.xhh.greymall.ssoserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GreymallTestSsoServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(GreymallTestSsoServerApplication.class, args);
    }

}
