package com.xhh.greymall.thirdparty.component;

import com.xhh.common.utils.OkHttpUtils;
import com.xhh.greymall.thirdparty.util.HttpUtils;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @description
 * @author: wei-xhh
 * @create: 2020-07-19
 **/
@Component
@Data
@ConfigurationProperties(prefix = "spring.alicloud.sms")
public class SmsComponent {

    private String host;
    private String path;
    private String skin;
    private String sign;
    private String appcode;

    public void sendSmsCode(String phone, String code){
        String appcode = "0db4401df355450385fa11bdd7300b3c"; // 【3】开通服务后 买家中心-查看AppCode
        Map<String,String> map = new HashMap<>();
        map.put("code", code);
        map.put("phone", phone);
        map.put("skin", skin);
        map.put("sign", sign);

//        String s = HttpUtils.doGet(host, path, appcode, map);
        String s = "";
        try {
            s = OkHttpUtils.doGet(host, path, appcode, map);
        } catch (IOException e) {
            System.out.println("请求短信验证码出错：" + e.toString());
        }
        System.out.println(s);
    }
}
