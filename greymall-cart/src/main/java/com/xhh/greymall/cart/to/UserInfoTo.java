package com.xhh.greymall.cart.to;

import lombok.Data;
import lombok.ToString;

/**
 * @description:
 * @author: wei-xhh
 * @create: 2020-07-24
 */
@ToString
@Data
public class UserInfoTo {
    private Long userId;
    private String userKey;
    private boolean tempUser = false;
}
