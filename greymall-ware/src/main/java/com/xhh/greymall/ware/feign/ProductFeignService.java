package com.xhh.greymall.ware.feign;

import com.xhh.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @description
 * @author: weiXhh
 * @create: 2020-05-12 15:39
 **/
@FeignClient("greymall-product")
public interface ProductFeignService {
    /**
     * 1./product/skuinfo/info/{skuId}
     * 2.可以让所有请求过网关/api/product/skuinfo/info/{skuId}
     *
     * @param skuId
     * @return
     */
    @RequestMapping("/product/skuinfo/info/{skuId}")
    public R info(@PathVariable("skuId") Long skuId);
}
