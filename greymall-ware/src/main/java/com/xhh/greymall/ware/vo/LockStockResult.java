package com.xhh.greymall.ware.vo;

import lombok.Data;

/**
 * @description:
 * @author: wei-xhh
 * @create: 2020-07-28
 */
@Data
public class LockStockResult {
    private Long skuId; //
    private Integer num;
    private Boolean locked;
}
