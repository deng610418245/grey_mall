package com.xhh.greymall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xhh.common.to.mq.OrderTo;
import com.xhh.common.to.mq.StockLockedTo;
import com.xhh.common.utils.PageUtils;
import com.xhh.greymall.ware.entity.WareSkuEntity;
import com.xhh.greymall.ware.vo.LockStockResult;
import com.xhh.greymall.ware.vo.SkuHasStockVO;
import com.xhh.greymall.ware.vo.WareSkuLockVo;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author genghui
 * @email 484613733@qq.com
 * @date 2020-05-02 17:42:07
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void addStock(Long skuId, Long wareId, Integer skuNum);

    // 获取库存信息
    List<SkuHasStockVO> getSkuHasStock(List<Long> skuIds);

    boolean orderLockStock(WareSkuLockVo vo);

    void unlockStock(StockLockedTo to);

    void unlockStock(OrderTo orderTo);
}

