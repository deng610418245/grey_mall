package com.xhh.greymall.ware.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @description
 * @author: weiXhh
 * @create: 2020-05-12 13:45
 **/
@Data
public class PurchaseDoneVO {

    @NotNull
    private Long id; // 采购单id

    private List<PurchaseDoneItemVO> items;
}
