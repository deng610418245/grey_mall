package com.xhh.greymall.ware.vo;

import lombok.Data;

/**
 * @description
 * @author: weiXhh
 * @create: 2020-05-12 13:47
 **/
@Data
public class PurchaseDoneItemVO {
    private Long itemId;
    private Integer status;
    private String reason;
}
