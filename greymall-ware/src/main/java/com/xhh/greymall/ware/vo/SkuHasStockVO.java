package com.xhh.greymall.ware.vo;

import lombok.Data;

/**
 * @description
 * @author: weiXhh
 * @create: 2020-06-23 09:30
 **/
@Data
public class SkuHasStockVO {

    private Long skuId;
    private Boolean hasStock;
}
