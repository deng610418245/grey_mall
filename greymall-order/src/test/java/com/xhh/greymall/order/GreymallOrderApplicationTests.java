package com.xhh.greymall.order;

import com.xhh.greymall.order.entity.OrderEntity;
import com.xhh.greymall.order.entity.OrderReturnReasonEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.UUID;

@Slf4j
@SpringBootTest
class GreymallOrderApplicationTests {

    @Autowired
    AmqpAdmin amqpAdmin;

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Test
    void sendMessageTest() {
//        OrderReturnReasonEntity reasonEntity = new OrderReturnReasonEntity();
//        reasonEntity.setId(1L);
//        reasonEntity.setCreateTime(new Date());
//        reasonEntity.setName("哈哈");
        //1、发送消息,如果发送的消息是个对象，我们会使用序列化机制，将对象写出去。Serializable
//        String msg = "hello world";
        for (int i = 0; i < 10; i++) {
            if (i % 2 == 0) {
                OrderReturnReasonEntity reasonEntity = new OrderReturnReasonEntity();
                reasonEntity.setId(1L);
                reasonEntity.setCreateTime(new Date());
                reasonEntity.setName("哈哈-" + i);
                //2、发送的对象类型
                rabbitTemplate.convertAndSend("hello-java-exchange", "hello.java", reasonEntity);
            } else {
                OrderEntity orderEntity = new OrderEntity();
                orderEntity.setOrderSn(UUID.randomUUID().toString());
                rabbitTemplate.convertAndSend("hello-java-exchange", "hello.java", orderEntity);
            }
            log.info("消息发送完成");
        }
//        //2、发送的对象类型
//        rabbitTemplate.convertAndSend("hello-java-exchange","hello.java",reasonEntity);
//        log.info("消息发送完成{}",reasonEntity);
    }

    /**
     * String name, boolean durable, boolean autoDelete, Map<String, Object> arguments
     */
    @Test
    void createExchange() {
        //amqpAdmin
        //Exchange
        DirectExchange directExchange =
                new DirectExchange("hello-java-exchange", true, false);
        amqpAdmin.declareExchange(directExchange);
        log.info("Exchange[{}]创建成功", "hello-java-exchange");

    }

    /**
     * String name, boolean durable, boolean exclusive, boolean autoDelete,
     *
     * @Nullable Map<String, Object> arguments
     */
    @Test
    void createQueue() {
        Queue queue = new Queue("hello-java-queue", true, false, false);
        amqpAdmin.declareQueue(queue);
        log.info("Queue[{}]创建成功", "hello-java-queue");
    }

    /**
     * String destination, 【目的地】
     * DestinationType destinationType,【目的地类型】
     * String exchange, 【交换机】
     * String routingKey,【路由键】
     *
     * @Nullable Map<String, Object> arguments 【自定义参数】
     */
    @Test
    void createBinding() {
        Binding binding = new Binding(
                "hello-java-queue",
                Binding.DestinationType.QUEUE,
                "hello-java-exchange",
                "hello.java",
                null);
        amqpAdmin.declareBinding(binding);
        log.info("Binding[{}]创建成功", "hello-java-binding");
    }
}
