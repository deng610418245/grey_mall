package com.xhh.greymall.order.config;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.annotation.PostConstruct;

/**
 * @description:
 * @author: wei-xhh
 * @create: 2020-07-25
 */
@Configuration
public class MyRabbitConfig {

    RabbitTemplate rabbitTemplate;
//    public MyRabbitConfig(RabbitTemplate rabbitTemplate){
//        this.rabbitTemplate = rabbitTemplate;
//        initRabbitTemplate();
//    }
    //TODO 出现循环依赖
    @Primary
    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory){
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        this.rabbitTemplate = rabbitTemplate;
        rabbitTemplate.setMessageConverter(messageConverter());
        initRabbitTemplate();
        return rabbitTemplate;
    }

    @Bean
    public MessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }

    /**
     * 定制RabbitTemplate
     * 1、服务收到消息就回调
     *     1、spring.rabbitmq.publisher-confirm-type=correlated
     *     2、设置确认回调
     * 2、消息正确抵达队列进行回调
     *     1、spring.rabbitmq.publisher-returns=true
     *        spring.rabbitmq.template.mandatory=true
     *     2、设置确认回调
     *
     * 3、消费端确认(保证每个消息被正确消费，此时才可以broker删除这个消息)
     *     1、默认是自动确认的，只要消息接收到，客户端会自动确认，服务端就会移除这个消息
     *
     * @PostConstruct:构造器创建完成后执行这个方法
     */
//    @PostConstruct
    public void initRabbitTemplate(){
        //设置确认回调
        rabbitTemplate.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {
            /**
             *1、只要消息抵达Broker就ack=true
             * @param correlationData 当前消息的唯一关联数据（这个是消息的唯一id）
             * @param ack 消息是否成功收到
             * @param cause 失败的原因
             */
            @Override
            public void confirm(CorrelationData correlationData, boolean ack, String cause) {
                /**
                 * 1.做好消息确认机制
                 * 2.每一个发送的消息都在数据库做好记录，定期将失败的消息再次发送
                 */
                //RabbitMQ服务器收到了
                System.out.println("confirm...correlationData[" + correlationData + "]=>ack[" + ack + "]=>cause[" + cause + "]");
            }
        });

        //设置消息抵达队列的确认回调
        rabbitTemplate.setReturnCallback(new RabbitTemplate.ReturnCallback() {
            /**
             * 只要消息没有投递给指定的队列，就触发这个失败回调
             * @param message 投递失败的消息详细详细
             * @param replyCode 回复的状态码
             * @param replyText 回复的文本内容
             * @param exchange 当时这个消息发给哪个交换机
             * @param routingKey 当时这个消息用哪个路由键
             */
            @Override
            public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
                //报错误了，修改数据库当前消息的状态
                System.out.println("Fail Message[" + message + "]=>replyCode[" + replyCode + "]=>replyText[" + replyText + "]=>exchange[" + "]=>" + exchange + "]=>routingKey" + routingKey);
            }
        });
    }
}
