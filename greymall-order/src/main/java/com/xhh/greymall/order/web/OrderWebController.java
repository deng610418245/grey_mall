package com.xhh.greymall.order.web;

import com.xhh.greymall.order.service.OrderService;
import com.xhh.greymall.order.vo.OrderConfirmVo;
import com.xhh.greymall.order.vo.OrderSubmitVo;
import com.xhh.greymall.order.vo.SubmitOrderResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.concurrent.ExecutionException;

/**
 * @description:
 * @author: wei-xhh
 * @create: 2020-07-26
 */
@Controller
public class OrderWebController {

    @Autowired
    OrderService orderService;

    @GetMapping("/toTrade")
    public String toTrade(Model model) throws ExecutionException, InterruptedException {
        OrderConfirmVo confirmVo = orderService.confirmOrder();
        //展示订单确认的数据
        model.addAttribute("orderConfirmData", confirmVo);
        return "confirm";
    }

    @PostMapping("/submitOrder")
    public String submitOrder(OrderSubmitVo vo, Model model, RedirectAttributes attributes) {

        //下单：去创建订单，验令牌，验价格，锁库存
        SubmitOrderResponseVo responseVo = orderService.submitOrder(vo);

        System.out.println("订单提交的数据..." + vo);
        if (responseVo.getCode() == 0) {
            //下单成功来到支付选择页
            model.addAttribute("submitOrderResp", responseVo);
            return "pay";
        } else {
            String msg = "下单失败:";
            switch (responseVo.getCode()) {
                case 1:
                    msg += "订单信息过期，请刷新再提交";
                    break;
                case 2:
                    msg += "订单商品价格发生变化，请确认后再次提交";
                    break;
                case 3:
                    msg += "库存锁定失败，商品库存不足";
                    break;
            }
            //下单失败回到订单确认页
            attributes.addFlashAttribute("msg", msg);
            return "redirect:http://order.greymall.com/toTrade";
        }

    }
}
