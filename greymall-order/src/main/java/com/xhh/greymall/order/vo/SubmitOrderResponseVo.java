package com.xhh.greymall.order.vo;

import com.xhh.greymall.order.entity.OrderEntity;
import lombok.Data;

/**
 * @description:
 * @author: wei-xhh
 * @create: 2020-07-27
 */
@Data
public class SubmitOrderResponseVo {
    private OrderEntity order;
    private Integer code; //错误状态码:0成功

}
