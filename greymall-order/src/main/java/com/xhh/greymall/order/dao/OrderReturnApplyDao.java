package com.xhh.greymall.order.dao;

import com.xhh.greymall.order.entity.OrderReturnApplyEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单退货申请
 * 
 * @author genghui
 * @email 484613733@qq.com
 * @date 2020-05-02 17:35:15
 */
@Mapper
public interface OrderReturnApplyDao extends BaseMapper<OrderReturnApplyEntity> {
	
}
