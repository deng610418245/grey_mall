package com.xhh.greymall.order.service.impl;

import com.alibaba.fastjson.JSON;
import com.rabbitmq.client.Channel;
import com.xhh.greymall.order.entity.OrderEntity;
import com.xhh.greymall.order.entity.OrderReturnReasonEntity;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xhh.common.utils.PageUtils;
import com.xhh.common.utils.Query;

import com.xhh.greymall.order.dao.OrderItemDao;
import com.xhh.greymall.order.entity.OrderItemEntity;
import com.xhh.greymall.order.service.OrderItemService;

@RabbitListener(queues = {"hello-java-queue"})
@Service("orderItemService")
public class OrderItemServiceImpl extends ServiceImpl<OrderItemDao, OrderItemEntity> implements OrderItemService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderItemEntity> page = this.page(
                new Query<OrderItemEntity>().getPage(params),
                new QueryWrapper<OrderItemEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * queues：声明需要监听的所有队列
     *
     * org.springframework.amqp.core.Message
     *
     * 参数可以写以下类型
     * 1、Message message：原生消息详细详细。头+体
     * 2、T<发送的消息的类型> OrderReturnReasonEntity content;
     * 3、Channel channel：当前传输数据的通道
     *
     * Queue：可以很多人都来监听。只要收到消息，队列删除消息，而且只能有一个收到此消息
     *
     * 1、订单服务启动多个：同一个消息，只能有一个客户端能收到
     * 2、只有一个消息完全处理完，方法运行结束，我们就可以接收下一个消息
     */
//    @RabbitListener(queues = {"hello-java-queue"})
    @RabbitHandler
    public void receiveMessage(Message message,
                               OrderReturnReasonEntity content,
                               Channel channel){


        //消息头属性信息
        System.out.println("接收到消息" + content);
        MessageProperties messageProperties = message.getMessageProperties();
        System.out.println("消息处理完成-》" + content.getName());

        //channel内顺序自增的
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        System.out.println("deliveryTag==>" + deliveryTag);
        //签收货物,非批量模式
        try {
            if(deliveryTag %2 == 0){
                //收货
                channel.basicAck(deliveryTag,false);
                System.out.println("签收了货物..." + deliveryTag);
            } else {
                //退货 requeue=false 丢弃， requeue=true 发回服务器，服务器重新入队
                channel.basicNack(deliveryTag,false,true);
//                channel.basicReject();
                System.out.println("没有签收了货物..." + deliveryTag);
            }

        } catch (IOException e) {
            //网络中断
            e.printStackTrace();
        }
    }

    @RabbitHandler
    public void receiveMessage2(OrderEntity content){
        //消息头属性信息
        System.out.println("接收到消息" + content);
    }
}