[TOC]

* * *

# 灰灰商城-分布式高级篇-3

## Nginx+Windows搭建域名访问环境

### 正向代理
作用：隐藏客户端信息

例如：如果想访问谷歌，访问不上，可以搭建代理服务器，电脑配置代理服务器的地址，想访问某个地址，就由代理服务器帮我们访问

* * *

### 反向代理

作用：屏蔽内网服务器信息，负载均衡访问

例如：想访问我们的商城，我们的商城每个服务器由内网部署，如果整个内网暴露出去，容易引起攻击，为了能找到整个内网服务器集群，在它们前面前置一个服务器（这样就是反向代理），而这个服务器可以使用Nginx。

* * *

### 搭建域名访问环境

可以在window-hosts文件配置域名访问的ip地址

位置：
C:\Windows\System32\drivers\etc\hosts

window会先找系统的域名映射
如果没有，则会去网络上的DNS

可以使用SwitchHosts快速配置



 ![avatar](\images\switchhosts2.jpg)



下载地址

[https://github.com/oldj/SwitchHosts/releases](https://github.com/oldj/SwitchHosts/releases)


以管理员身份打开，如果还是不行

找到：C:\Windows\System32\drivers\etc\hosts

**打开hosts的属性：将只读去掉；**

 ![avatar](\images\switchhosts.jpg) 

之后就可以访问域名对应ip下的进程

* * *

### Nginx配置文件

nginx.conf->全局块：配置影响nginx全局的指令，如：用户组，nginx进程pid存放路径，日志存放路径，配置文件引入，允许生成worker process数等。

* * *

nginx.conf->events块：配置影响nginx服务器与用户的网络连接，如：每个进程的最大连接数，选取哪种事件驱动模型处理连接请求，是否允许同时接受多个网络连接，开启多个网络连接序列化等。

* * *

nginx.conf->http块：可以嵌套多个server，配置代理，缓存，日志定义等绝大多数功能和第三方模块的配置，如文件引入，mine-type定义，日志自定义，是否使用sendifie传输文件，连接超时时间，单连接请求数等。

* * *

http块->http全局块：如upstream，错误页面，连接超时等。

* * *

http块->server块：配置虚拟主机的相关参数，一个http中可以有多个server.

* * *

http块->location：配置请求的路由；以及各种页面的处理情况。

* * *
如图：

 ![avatar](\images\nginx配置文件.jpg)

* * *


### 反向代理配置

在conf.d下

cp default.conf greymall.conf

vi greymall.conf

修改如下配置

 ![avatar](\images\nginx配置文件2.jpg)

访问域名 greymall.com

* * *

### Nginx请求网关

- 负载均衡配置

文档：

[http://nginx.org/en/docs/http/load_balancing.html](http://nginx.org/en/docs/http/load_balancing.html)

**vi nginx.conf**

![avatar](\images\nginx配置负载均衡.jpg)

**vi greymall.conf**

![avatar](\images\nginx配置负载均衡2.jpg)



* * *


- 配置网关路由规则

在greymall-gateway微服务配置文件下配置

```yml
- id: greymall_host_route    
    uri: lb://greymall-product    
    predicates:      
        - Host=**.greymall.com,greymall.com
```

配置后访问失败：

原因：主机地址没有匹配上，nginx代理给网关的时候，会丢掉host信息。

修改：配置nginx,
vi greymall.conf

设置：proxy_set_header Host $host

![avatar](\images\nginx配置负载均衡3.jpg)