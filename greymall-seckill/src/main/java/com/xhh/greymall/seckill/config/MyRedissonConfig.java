package com.xhh.greymall.seckill.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * @description
 * @author: weiXhh
 * @create: 2020-07-01 08:46
 **/
@Configuration
public class MyRedissonConfig {

    /**
     * 所有Redisson的使用通过RedissonClient
     * @return
     * @throws IOException
     */
    @Bean(destroyMethod="shutdown")
    public RedissonClient redisson(@Value("${spring.redis.host}") String url) throws IOException {

        Config config = new Config();
        config.useSingleServer().setAddress("redis://"+url+":6379");
        RedissonClient redissonClient = Redisson.create(config);

        return redissonClient;
    }

}
