package com.xhh.greymall.auth.vo;

import lombok.Data;

/**
 * @description:
 * @author: wei-xhh
 * @create: 2020-07-20
 */
@Data
public class UserLoginVo {
    private String loginacct;
    private String password;
}
