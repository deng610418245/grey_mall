package com.xhh.greymall.auth.vo;

import lombok.Data;

/**
 * @description: 社交登录json
 * @author: wei-xhh
 * @create: 2020-07-21
 */
@Data
public class SocialUser {
    private String access_token;
    private String remind_in;
    private Long expires_in;
    private String uid;
    private String isRealName;
}
